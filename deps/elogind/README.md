# WARNING WARNING WARNING

Remove ConsoleKit2 before installing elogind

  root> removepkg ConsoleKit2

Remember to reinstall ConsoleKit2 if you uninstall elogind.

# PAM

To use elogind you will need to **MODIFIED** slackware default PAM configuration,

file: /etc/pam.d/system-auth

With the session lines from elogind system-auth template

file: /usr/share/factory/etc/pam.d/system-auth

But in my small knowledge, adding just one line like below at the bottom
of /etc/pam.d/system-auth is sufficient:

-session    optional     pam_elogind.so

If you have a trouble logging in from remote (ie. remote SSH) because
of login session tracking then you can add pam_elogind.so to /etc/pam.d/login
instead of global system-auth. That way elogind will only tracking local
login and excludes tracking remote login.

# Installation

Default elogind libexec dir is /lib$LIBDIRSUFFIX/elogind.
To change it, use this option:

ELOGINDLIBEXECDIR=/somedir/elogind ./elogind.SlackBuild

Default cgroup controller if not specified is **elogind**.
To use other cgroup controller, for example openrc, use CGCONTROLLER option:

CGCONTROLLER=openrc ./elogind.SlackBuild

# NOTE:

You **MUST** install and **BOOT** to openrc before using it as cgroup controller.
One reason is why bother building elogind with openrc support if you are not
going to use it?

# Manual Pages

Manual pages is not included in default installation. To build and install
manual pages, use this option:

INSTALL_MANPAGES=true ./elogind.SlackBuild

# Usage

You can leave elogind out of any rc init script or runlevel.
It will then be started automatically when dbus is started.

Or start elogind-daemon from rc.local

if [ -x /etc/rc.d/rc.elogind ]; then
  /etc/rc.d/rc.elogind start
fi

Or more dirtier, you can replace startup line of rc.consolekit
in /etc/rc.d/rc.M with rc.elogind.
